package ee.sda.sdaCodingLessons;

import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;

public class ArrayTasks {
   // helper method
   private static int[] intSet2IntArray(Set<Integer> s) {
      int[] retValue = new int[s.size()];
      int j = 0;
      for (Integer i : s) retValue[j++] = i;
      return retValue;
   }

   // Task 1
   private static void sortArray(int[] arr) {
      int tmp;
      for (int i=0; i < arr.length-1; i++) {
         for (int j=i+1; j < arr.length; j++) {
            if (arr[i] > arr[j]) {
               tmp = arr[i];
               arr[i] = arr[j];
               arr[j] = tmp;
            }
         }
      }
   }

   // Task 1
   private static void sortArray(String[] arr) {
      String tmp;
      for (int i=0; i < arr.length-1; i++) {
         for (int j=i+1; j < arr.length; j++) {
            if (arr[i].compareTo(arr[j]) > 0) {
               tmp = arr[i];
               arr[i] = arr[j];
               arr[j] = tmp;
            }
         }
      }
   }

   // Task 2
   private static int arraySum(int[] arr) {
      int sum = 0;
      for (int i : arr) sum += i;
      return sum;
   }

   // Task 3
   private static void printGrid() {
      for (int i=0; i < 10; i++) {
         for (int j=0; j < 9; j++) {
            System.out.print("- ");
         }
         System.out.println("-");
      }
   }

   // Task 4
   private static double calculateAverage(int[] arr) {
      return arraySum(arr)/(double)arr.length;
   }

   // Task 5
   private static boolean contains(int[] arr, int value) {
      for (int i : arr) {
         if (i == value) return true;
      }
      return false;
   }

   // Task 5
   private static boolean contains(String[] arr, String value) {
      for (String s : arr) {
         if (s.equals(value)) return true;
      }
      return false;
   }

   // Task 6
   private static int elementIndex(int[] arr, int value) {
      for (int i = 0; i < arr.length; i++) {
         if (arr[i] == value) return i;
      }
      return -1;
   }

   // Task 6
   private static int elementIndex(String[] arr, String value) {
      for (int i = 0; i < arr.length; i++) {
         if (value.equals(arr[i])) return i;
      }
      return -1;
   }

   // Task 7
   private static int[] removeElement(int[] arr, int index) {
      if (arr == null || index < 0 || index >= arr.length) return arr;
      int[] returnArray = new int[arr.length-1];
      for (int i = 0, j = 0; i < arr.length; i++) {
         if (i == index) continue;
         returnArray[j++] = arr[i];
      }
      return returnArray;
   }

   // Task 7
   private static String[] removeElement(String[] arr, int index) {
      if (arr == null || index < 0 || index >= arr.length) return arr;
      String[] returnArray = new String[arr.length-1];
      for (int i = 0, j = 0; i < arr.length; i++) {
         if (i == index) continue;
         returnArray[j++] = arr[i];
      }
      return returnArray;
   }

   // Task 8
   private static void copyArray(int[] src, int[] dest) {
      for (int i=0; i < src.length; i++) dest[i] = src[i];
   }

   // Task 8
   private static void copyArray(String[] src, String[] dest) {
      for (int i=0; i < src.length; i++) dest[i] = src[i];
   }

   // Task 9
   private static int[] insertElement(int[] arr, int value, int index) {
      int[] returnArray = new int[arr.length+1];
      for (int i = returnArray.length-1; i > index; i--) returnArray[i] = arr[i-1];
      returnArray[index] = value;
      for (int i = index-1; i >= 0; i--) returnArray[i] = arr[i];
      return returnArray;
   }

   // Task 9
   private static String[] insertElement(String[] arr, String value, int index) {
      String[] returnArray = new String[arr.length+1];
      for (int i = returnArray.length-1; i > index; i--) returnArray[i] = arr[i-1];
      returnArray[index] = value;
      for (int i = index-1; i >= 0; i--) returnArray[i] = arr[i];
      return returnArray;
   }

   // Task 10
   private static int maxValue(int[] arr) {
      int max = Integer.MIN_VALUE;
      for (int i : arr) {
         if (i > max) max = i;
      }
      return max;
   }

   // Task 10
   private static int minValue(int[] arr) {
      int min = Integer.MAX_VALUE;
      for (int i : arr) {
         if (i < min) min = i;
      }
      return min;
   }

   // Task 11
   private static int[] reverseArray(int[] arr) {
      int[] retArr = new int[arr.length];
      for (int i = 0, j = arr.length-1; i < arr.length; i++, j--) retArr[j] = arr[i];
      return retArr;
   }

   // Task 12
   private static int[] findDuplicates(int[] arr) {
      Set<Integer> intSet = new HashSet<>();
      for (int i = 0; i < arr.length-1; i++) {
         for (int j = i+1; j < arr.length; j++) {
            if (arr[i] == arr[j]) {
               intSet.add(arr[i]);
               break;
            }
         }
      }
      return intSet2IntArray(intSet);
   }

   // Task 13
   private static String[] findDuplicates(String[] arr) {
      Set<String> stringSet = new HashSet<>();
      for (int i = 0; i < arr.length-1; i++) {
         for (int j = i+1; j < arr.length; j++) {
            if (arr[i].equals(arr[j])) {
               stringSet.add(arr[i]);
               break;
            }
         }
      }
      return stringSet.toArray(new String[0]);
   }

   // Task 14
   private static String[] findCommonElements(String[] arr1, String[] arr2) {
      Set<String> stringSet = new HashSet<>();
      for (String s : arr1) {
         for (String value : arr2) {
            if (s.equals(value)) {
               stringSet.add(s);
               break;
            }
         }
      }
      return stringSet.toArray(new String[0]);
   }

   // Task 15
   private static int[] findCommonElements(int[] arr1, int[] arr2) {
      Set<Integer> intSet = new HashSet<>();
      for (int k : arr1) {
         for (int i : arr2) {
            if (k == i) {
               intSet.add(k);
               break;
            }
         }
      }
      return intSet2IntArray(intSet);
   }

   // Task 16
   private static int[] removeDuplicates(int[] arr) {
      int end = arr.length;
      int[] arrTmp = new int[end];
      copyArray(arr, arrTmp);
      for (int i = 0; i < end; i++) {
         for (int j = i+1; j < end; j++) {
            if (arrTmp[i] == arrTmp[j]) {
               int shiftLeft = j;
               for (int k = j+1; k < end; k++, shiftLeft++) {
                  arrTmp[shiftLeft] = arrTmp[k];
               }
               end--;
               j--;
            }
         }
      }
      int[] retStr = new int[end];
      for (int i = 0; i < end; i++) retStr[i] = arrTmp[i];
      return retStr;
   }

   // Task 16
   private static String[] removeDuplicates(String[] arr) {
      int end = arr.length;
      String[] arrTmp = new String[end];
      copyArray(arr, arrTmp);
      for (int i = 0; i < end; i++) {
         for (int j = i+1; j < end; j++) {
            if (arrTmp[i].equals(arrTmp[j])) {
               int shiftLeft = j;
               for (int k = j+1; k < end; k++, shiftLeft++) {
                  arrTmp[shiftLeft] = arrTmp[k];
               }
               end--;
               j--;
            }
         }
      }
      String[] retStr = new String[end];
      for (int i = 0; i < end; i++) retStr[i] = arrTmp[i];
      return retStr;
   }

   // Task 17
   private static int secondLargestValue(int[] arr) {
      int[] arrTmp = new int[arr.length];
      copyArray(arr, arrTmp);
      sortArray(arrTmp);
      int index = arr.length - 1;
      while (index > 0 && arrTmp[index] == arrTmp[arr.length-1]) index--;
      return arrTmp[index];
   }

   // Task 18
   private static int secondSmallestValue(int[] arr) {
      int[] arrTmp = new int[arr.length];
      copyArray(arr, arrTmp);
      sortArray(arrTmp);
      int index = 0;
      while (index < arr.length-1 && arrTmp[index] == arrTmp[0]) index++;
      return arrTmp[index];
   }

   // Task 19
   private static int[][] matrixSum(int[][] matrix1, int[][] matrix2) {
      int nrows = matrix1.length;
      int ncols = matrix1[0].length;
      int[][] tmpMatrix = new int[nrows][ncols];
      for (int i = 0; i < nrows; i++) {
         for (int j = 0; j < ncols; j++) {
            tmpMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
         }
      }
      return tmpMatrix;
   }

   // Task 21
   private static String[] arrayList2StringArray(ArrayList<String> al) {
      return al.toArray(new String[al.size()]);
   }

   // Task 22
   private static void pairsValue(int[] arr, int number) {
      for (int i = 0; i < arr.length-1; i++) {
         for (int j = i+1; j < arr.length; j++) {
            if (arr[i] + arr[j] == number) {
               System.out.println(arr[i] + " " + arr[j]);
            }
         }
      }
   }

   // Task 23
   private static boolean arraysEqual(int[] arr1, int[] arr2) {
      boolean equal = true;
      if (arr1.length == arr2.length) {
         for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
               equal = false;
               break;
            }
         }
      } else {
         equal = false;
      }
      return equal;
   }

   // Task 24
   private static int missingNumber(int[] arr) {
      int number = 0;
      int[] arrTmp = new int[arr.length];
      copyArray(arr, arrTmp);
      sortArray(arrTmp);
      int i = 0;
      for (; i < arrTmp.length-1; i++) {
         if (arrTmp[i+1] - arrTmp[i] != 1) break;
      }
      if (i < arrTmp.length - 1) {
         number = arrTmp[i] + 1;
      }
      return number;
   }

   // Task 25
   private static int[] commonOf3Arrays(int[] arr1, int[] arr2, int[] arr3) {
      int i1 = 0, i2 = 0, i3 = 0;
      Set<Integer> intSet = new HashSet<>();
      while (i1 < arr1.length && i2 < arr2.length && i3 < arr3.length) {
         if (arr1[i1] == arr2[i2] && arr1[i1] == arr3[i3]) {
            intSet.add(arr1[i1]);
            i1++;
            i2++;
            i3++;
         } else if (arr1[i1] < arr2[i2]) {
            i1++;
         } else if (arr2[i2] < arr3[i3]) {
            i2++;
         } else {
            i3++;
         }
      }
      return intSet2IntArray(intSet);
   }

   public static void main(String[] args) {
      int[] numbers = {0, 1, 3, 1};
      int[] numbers2 = {1, 2, 4};
      String[] strings = {"Java", "Python", "Java"};
      String[] strings2 = {"C++", "Java"};
      int[][] matrix1 = {
                           {1, 2, 3},
                           {4, 5, 6}
                        };
      int[][] matrix2 = {
                           {4, 5, 6},
                           {1, 2, 3}
                        };

      int[] numbersTmp = new int[numbers.length];
      copyArray(numbers, numbersTmp);

      String[] stringsTmp = new String[strings.length];
      copyArray(strings, stringsTmp);

      sortArray(numbersTmp);
      System.out.println("Sorted int array:");
      System.out.println(Arrays.toString(numbersTmp));
      System.out.println();

      sortArray(stringsTmp);
      System.out.println("Sorted string array:");
      System.out.println(Arrays.toString(stringsTmp));
      System.out.println();

      System.out.println("Sum of int array values: " + arraySum(numbers));
      System.out.println();

      System.out.println("Grid:");
      printGrid();
      System.out.println();

      System.out.println("Average of int array values: " + calculateAverage(numbers));
      System.out.println();

      System.out.println("Int array contains 3: " + contains(numbers, 3));
      System.out.println("Int array contains 4: " + contains(numbers, 4));
      System.out.println("String array contains \"Python\": " + contains(strings, "Python"));
      System.out.println("String array contains \"cde\": " + contains(strings, "cde"));
      System.out.println();

      System.out.println("Index of int array element 3: " + elementIndex(numbers, 3));
      System.out.println("Index of int array element 4: " + elementIndex(numbers, 4));
      System.out.println("Index of String array element \"Python\": " + elementIndex(strings, "Python"));
      System.out.println("Index of String array element \"cde\": " + elementIndex(strings, "cde"));
      System.out.println();

      System.out.println("Original array: " + Arrays.toString(numbers));
      System.out.println("Resultant array after removing index 0: " + Arrays.toString(removeElement(numbers, 0)));
      System.out.println("Resultant array after inserting element 5 at index 1: " + Arrays.toString(insertElement(numbers, 5, 1)));
      System.out.println();
      System.out.println("Original array: " + Arrays.toString(strings));
      System.out.println("Resultant array after removing index 0: " + Arrays.toString(removeElement(strings, 0)));
      System.out.println("Resultant array after inserting element \"cde\" at index 1: " + Arrays.toString(insertElement(strings, "cde", 1)));
      System.out.println();

      System.out.println("Minimum of int array values: " + minValue(numbers));
      System.out.println("Maximum of int array values: " + maxValue(numbers));
      System.out.println();

      System.out.println("Reversed int array: " + Arrays.toString(reverseArray(numbers)));
      System.out.println();

      System.out.println("Duplicates in int array: " + Arrays.toString(findDuplicates(numbers)));
      System.out.println();

      System.out.println("Duplicates in String array: " + Arrays.toString(findDuplicates(strings)));
      System.out.println();

      System.out.println("Common elements of int arrays:");
      System.out.println("Array 1: " + Arrays.toString(numbers));
      System.out.println("Array 2: " + Arrays.toString(numbers2));
      System.out.println(Arrays.toString(findCommonElements(numbers, numbers2)));
      System.out.println();

      System.out.println("Common elements of String arrays:");
      System.out.println("Array 1: " + Arrays.toString(strings));
      System.out.println("Array 2: " + Arrays.toString(strings2));
      System.out.println(Arrays.toString(findCommonElements(strings, strings2)));
      System.out.println();

      System.out.println("Int array after duplicates removal: " + Arrays.toString(removeDuplicates(numbers)));
      System.out.println();

      System.out.println("String array after duplicates removal: " + Arrays.toString(removeDuplicates(strings)));
      System.out.println();

      System.out.println("Second largest value of int array: " + secondLargestValue(numbers));
      System.out.println();

      System.out.println("Second smallest value of int array: " + secondSmallestValue(numbers));
      System.out.println();

      System.out.println("Matrix 1: " + Arrays.deepToString(matrix1));
      System.out.println("Matrix 2: " + Arrays.deepToString(matrix2));
      System.out.println("Sum of 2 matrices: " + Arrays.deepToString(matrixSum(matrix1, matrix2)));
      System.out.println();

      // Task 20
      ArrayList<String> list = new ArrayList<>(Arrays.asList(strings));
      System.out.println("String array converted to ArrayList: " + list);
      System.out.println();

      System.out.println("ArrayList converted to String array: " + Arrays.toString(arrayList2StringArray(list)));
      System.out.println();

      System.out.println("Pairs of int array elements having sum 4");
      pairsValue(numbers, 4);
      System.out.println();

      System.out.println("Int arrays 1 and 2 are equal: " + arraysEqual(numbers, numbers2));
      System.out.println();

      System.out.println("Missing number in int array 2: " + missingNumber(numbers2));
      System.out.println();

      System.out.println("Common elements of 3 sorted int arrays: " + Arrays.toString(commonOf3Arrays(new int[]{1,2,3}, new int[]{3,4,5}, new int[]{3,5,7})));
   }
}
