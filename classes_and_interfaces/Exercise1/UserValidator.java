import java.util.regex.Pattern;

public class UserValidator {
   public static final Pattern EMAIL_VALIDATION_PATTERN = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");

   public static String[] validateEmails(String emailText, String alternativeEmailText) {
      class Email {
         String validatedEmail = null;

         Email(String email) {
            if (email == null || email.isEmpty() || !EMAIL_VALIDATION_PATTERN.matcher(email).matches())
               validatedEmail = "unknown";
            else
               validatedEmail = email;
         }

         public String getValidatedEmail() {
            return validatedEmail;
         }
      }
      Email email = new Email(emailText);
      Email alternativeEmail = new Email(alternativeEmailText);
      return new String[]{email.getValidatedEmail(), alternativeEmail.getValidatedEmail()};
   }
}
