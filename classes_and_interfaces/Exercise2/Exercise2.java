import java.time.Year;

public class Exercise2 {
   public static void main(String[] args) {
      Movie m = new Movie.MovieCreator()
                      .setTitle("Title")
                      .setDirector("Director")
                      .setYearOfRelease(Year.of(2000))
                      .setGenre("Genre")
                      .setDistributor("Distributor")
                      .createMovie();
      System.out.println(m);
   }
}
