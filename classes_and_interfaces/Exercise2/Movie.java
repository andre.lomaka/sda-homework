import java.time.Year;

public class Movie {
   private String title;
   private String director;
   private Year yearOfRelease;
   private String genre;
   private String distributor;

   public Movie(String title, String director, Year year, String genre, String distributor) {
      this.title = title;
      this.director = director;
      yearOfRelease = year;
      this.genre = genre;
      this.distributor = distributor;
   }

   public String getTitle() {
      return title;
   }

   public String getDirector() {
      return director;
   }

   public Year getYearOfRelease() {
      return yearOfRelease;
   }

   public String getGenre() {
      return genre;
   }

   public String getDistributor() {
      return distributor;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public void setDirector(String director) {
      this.director = director;
   }

   public void setYearOfRelease(Year year) {
      yearOfRelease = year;
   }

   public void setGenre(String genre) {
      this.genre = genre;
   }

   public void setDistributor(String distributor) {
      this.distributor = distributor;
   }

   public String toString() {
      return "Movie data:\nTitle: " + title + "\nDirector: " +
             director + "\nYear: " + yearOfRelease + "\nGenre: " +
             genre + "\nDistributor: " + distributor;
   }

   static class MovieCreator {
      private String title;
      private String director;
      private Year yearOfRelease;
      private String genre;
      private String distributor;

      public MovieCreator setTitle(String title) {
         this.title = title;
         return this;
      }

      public MovieCreator setDirector(String director) {
         this.director = director;
         return this;
      }

      public MovieCreator setYearOfRelease(Year year) {
         yearOfRelease = year;
         return this;
      }

      public MovieCreator setGenre(String genre) {
         this.genre = genre;
         return this;
      }

      public MovieCreator setDistributor(String distributor) {
         this.distributor = distributor;
         return this;
      }

      public Movie createMovie() {
         return new Movie(title, director, yearOfRelease, genre, distributor);
      }
   }
}
