public class Car {
   private String make;
   private String type = "";

   public String getMake() {
      return make;
   }

   public String getType() {
      return type;
   }

   public void setMake(String make) {
      this.make = make;
   }

   public void setType(String type) {
      this.type = type;
   }

   class Engine {
      private String type;

      public void setType() {
         switch (Car.this.type)
         {
            case "economy":
               type = "diesel";
               break;
            case "luxury":
               type = "electric";
               break;
            default:
               type = "petrol";
               break;
         }
      }

      public String getType() {
         return type;
      }
   }
}
