public class Exercise4 {
   public static void main(String[] args) {
      User user = new User();
      user.setName("Name1", new Validator<String>() {
                               public boolean validate(String s) {
                                  return (s != null &&
                                          !s.isEmpty() &&
                                          Character.isUpperCase(s.charAt(0)));
                               }
                            });
      user.setLastName("Lastname1", new Validator<String>() {
                               public boolean validate(String s) {
                                  return (s != null &&
                                          !s.isEmpty() &&
                                          Character.isUpperCase(s.charAt(0)));
                               }
                            });
      user.setAge(10, new Validator<Integer>() {
                               public boolean validate(Integer i) {
                                  return (i >= 0 && i <= 150);
                               }
                            });
      user.setLogin("0123456789", new Validator<String>() {
                               public boolean validate(String s) {
                                  return (s.length() == 10);
                               }
                            });
      user.setPassword("!qwerty", new Validator<String>() {
                               public boolean validate(String s) {
                                  return (s.contains("!"));
                               }
                            });
      System.out.println(user);
   }
}
