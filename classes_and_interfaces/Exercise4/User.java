public class User {
   private String name;
   private String lastName;
   private int age;
   private String login;
   private String password;

   public User() {
   }

   public void setName(String name, Validator<String> v) {
      if (v.validate(name)) this.name = name;
   }

   public void setLastName(String lastName, Validator<String> v) {
      if (v.validate(lastName)) this.lastName = lastName;
   }

   public void setAge(int age, Validator<Integer> v) {
      if (v.validate(age)) this.age = age;
   }

   public void setLogin(String login, Validator<String> v) {
      if (v.validate(login)) this.login = login;
   }

   public void setPassword(String password, Validator<String> v) {
      if (v.validate(password)) this.password = password;
   }

   public String getName() {
      return name;
   }

   public String getLastName() {
      return lastName;
   }

   public int getAge() {
      return age;
   }

   public String getLogin() {
      return login;
   }

   public String getPassword() {
      return password;
   }

   public String toString() {
      return "User{name='" + name + "' lastName='" + lastName
              + "' age='" + age + "' login='" + login
              + "' password='" + password + "'}";
   }
}
