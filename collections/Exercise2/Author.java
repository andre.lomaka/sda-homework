import java.util.Objects;

public class Author {
   private String name;
   private String lastName;
   private char gender;

   public Author(String name, String lastName, char gender) {
      this.name = name;
      this.lastName = lastName;
      this.gender = gender;
   }

   public void setName(String name) {
      this.name = name;
   }

   public void setLastname(String lastName) {
      this.lastName = lastName;
   }

   public void setGender(char gender) {
      this.gender = gender;
   }

   public String getName() {
      return name;
   }

   public String getLastName() {
      return lastName;
   }

   public char getGender() {
      return gender;
   }

   public boolean equals(Object obj) {
      if (this == obj) return true;
      if (obj instanceof Author) {
         Author a = (Author)obj;
         return name.equals(a.getName()) &&
                            lastName.equals(a.getLastName()) &&
                            gender == a.getGender();
      }
      else {
         return false;
      }
   }

   public int hashCode() {
      return Objects.hash(name, lastName, gender);
   }

   public String toString() {
      return name + " " + lastName + " " + gender;
   }
}
