import java.time.Year;
import java.util.List;
import java.util.Objects;

public class Book implements Comparable<Book> {
   private String title;
   private float price;
   private Year yearOfRelease;
   private List<Author> authorList;
   private Genre genre;

   public Book(String title, float price, Year yearOfRelease, List<Author> authorList, Genre genre) {
      this.title = title;
      this.price = price;
      this.yearOfRelease = yearOfRelease;
      this.authorList = authorList;
      this.genre = genre;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public void setPrice(float price) {
      this.price = price;
   }

   public void setYearOfRelease(Year yearOfRelease) {
      this.yearOfRelease = yearOfRelease;
   }

   public void setAuthorList(List<Author> authorList) {
      this.authorList = authorList;
   }

   public void setGenre(Genre genre) {
      this.genre = genre;
   }

   public String getTitle() {
      return title;
   }

   public float getPrice() {
      return price;
   }

   public Year getYearOfRelease() {
      return yearOfRelease;
   }

   public List<Author> getAuthorList() {
      return authorList;
   }

   public Genre getGenre() {
      return genre;
   }

   public boolean equals(Object obj) {
      if (this == obj) return true;
      if (obj instanceof Book) {
         Book b = (Book)obj;
         return title.equals(b.getTitle()) && price == b.getPrice() &&
                             yearOfRelease.equals(b.yearOfRelease) &&
                             authorList.equals(b.authorList) && genre == b.genre;
      }
      else {
         return false;
      }
   }

   public int hashCode() {
      return Objects.hash(title, price, yearOfRelease, authorList, genre);
   }

   public int compareTo(Book book) {
      return title.compareTo(book.getTitle());
   }

   public String toString() {
      String result = "title: " + title + "\nprice: " + price + "\nyear: " +
                       yearOfRelease + "\nAuthors:\n";
      for (Author a : authorList) {
         result += a;
         result += "\n";
      }
      result += "genre: ";
      result += genre;
      result += "\n";
      return result;
   }
}
