import java.util.List;
import java.util.ArrayList;
import java.time.Year;
import java.util.Collections;

public class BookService {
   private List<Book> books = new ArrayList<>();

   public void add(Book book) {
      books.add(book);
   }

   public void remove(Book book) {
      books.remove(book);
   }

   public List<Book> getAllBooks() {
      return books;
   }

   public List<Book> getBooksByGenre(Genre genre) {
      List<Book> result = new ArrayList<>();
      for (Book b : books) {
         if (b.getGenre() == genre) result.add(b);
      }
      return result;
   }

   public List<Book> getBooksOlderThan(Year year) {
      List<Book> result = new ArrayList<>();
      for (Book b : books) {
         if (year.compareTo(b.getYearOfRelease()) > 0) result.add(b);
      }
      return result;
   }

   public Book getMostExpensiveBook() {
      Book ret = null;
      float max = Float.MIN_VALUE;
      for (Book b : books) {
         if (b.getPrice() > max) {
            max = b.getPrice();
            ret = b;
         }
      }
      return ret;
   }

   public Book getCheapestBook() {
      Book ret = null;
      float min = Float.MAX_VALUE;
      for (Book b : books) {
         if (b.getPrice() < min) {
            min = b.getPrice();
            ret = b;
         }
      }
      return ret;
   }

   public List<Book> getByNumberOfAuthors(int numberOfAuthors) {
      List<Book> result = new ArrayList<>();
      for (Book b : books) {
         if (b.getAuthorList().size() == numberOfAuthors) {
            result.add(b);
         }
      }
      return result;
   }

   public List<Book> sortByTitleAsc() {
      Collections.sort(books);
      return books;
   }

   public List<Book> sortByTitleDesc() {
      Collections.sort(books, Collections.reverseOrder());
      return books;
   }

   public boolean isOnTheList(Book book) {
      return books.contains(book);
   }

   public List<Book> getByAuthor(Author author) {
      List<Book> result = new ArrayList<>();
      for (Book b : books) {
         if (b.getAuthorList().contains(author)) {
            result.add(b);
         }
      }
      return result;
   }
}
