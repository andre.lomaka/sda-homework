import java.time.Year;
import java.util.List;
import java.util.ArrayList;

public class Exercise2 {
   public static void main(String[] args) {
      BookService bookservice = new BookService();
      bookservice.add(new Book("Title1", 3.2f, Year.of(1985),
                      new ArrayList<>(List.of(new Author("Name1", "Lastname1", 'M'),
                                              new Author("Name2", "Lastname2", 'M'))
                                     ), Genre.ACTION));
      bookservice.add(new Book("Title2", 2.1f, Year.of(2019),
                      new ArrayList<>(List.of(new Author("Name3", "Lastname3", 'M'))
                                     ), Genre.FANTASY));
      bookservice.add(new Book("Title3", 1.1f, Year.of(2015),
                      new ArrayList<>(List.of(new Author("Name5", "Lastname5", 'F'),
                                              new Author("Name6", "Lastname6", 'F'))
                                     ), Genre.DRAMA));
      List<Book> books = bookservice.getAllBooks();
      for (Book b : books) {
         System.out.println(b);
      }
      System.out.println("Fantasy genre books:");
      books = bookservice.getBooksByGenre(Genre.FANTASY);
      for (Book b : books) {
         System.out.println(b);
      }
      System.out.println("Books older than 1999:");
      books = bookservice.getBooksOlderThan(Year.of(1999));
      for (Book b : books) {
         System.out.println(b);
      }
      System.out.println("Most expensive book:");
      System.out.println(bookservice.getMostExpensiveBook());
      System.out.println("Cheapest book:");
      System.out.println(bookservice.getCheapestBook());
      System.out.println("Books with 1 author:");
      books = bookservice.getByNumberOfAuthors(1);
      for (Book b : books) {
         System.out.println(b);
      }
      System.out.println("Books sorted by title(asc):");
      books = bookservice.sortByTitleAsc();
      for (Book b : books) {
         System.out.println(b);
      }
      System.out.println("Books sorted by title(desc):");
      books = bookservice.sortByTitleDesc();
      for (Book b : books) {
         System.out.println(b);
      }
      System.out.println("Books authored by Name5 Lastname5:");
      books = bookservice.getByAuthor(new Author("Name5", "Lastname5", 'F'));
      for (Book b : books) {
         System.out.println(b);
      }
      Book expensiveBook = bookservice.getMostExpensiveBook();
      System.out.println("Book on the list: " + bookservice.isOnTheList(expensiveBook));
      bookservice.remove(expensiveBook);
      System.out.println("Book on the list: " + bookservice.isOnTheList(expensiveBook));
      books = bookservice.getAllBooks();
      for (Book b : books) {
         System.out.println(b);
      }
   }
}
