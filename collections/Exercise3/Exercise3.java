import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

public class Exercise3 {
   public static void main(String[] args) {
      Random rand = new Random(10);
      int[] intArray = new int[100]; 
      for (int i = 0; i < 100; i++) {
         intArray[i] = rand.nextInt(50);
      }
      List<Integer> repeatedList = new ArrayList<>();
      List<Integer> uniqueList = new ArrayList<>();
      Set<Integer> intSet = new HashSet<>();
      Set<Integer> repeatedSet = new HashSet<>();
      System.out.println("Original array:");
      for (int i : intArray) {
         System.out.println(i);
         if (intSet.add(i)) uniqueList.add(i);
         else if (repeatedSet.add(i)) repeatedList.add(i);
      }
      System.out.println("Unique list:");
      for (int i : uniqueList) {
         System.out.println(i);
      }
      System.out.println("Repeated list:");
      for (int i : repeatedList) {
         System.out.println(i);
      }
   }
}
