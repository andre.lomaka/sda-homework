public enum Weekday {
   MONDAY,
   TUESDAY,
   WEDNESDAY,
   THURSDAY,
   FRIDAY,
   SATURDAY,
   SUNDAY;

   public boolean isWeekday() {
      switch (this) {
         case MONDAY:
         case TUESDAY:
         case WEDNESDAY:
         case THURSDAY:
         case FRIDAY:
            return true;
      }
      return false;
   }

   public boolean isHoliday() {
      return !isWeekday();
   }

   public void whichIsGreater(Weekday wd) {
      if (compareTo(wd) < 0)
         System.out.println(wd + " succeeds " + this);
      else if (compareTo(wd) > 0)
         System.out.println(wd + " precedes " + this);
      else
         System.out.println("Days are equal");
   }
}
