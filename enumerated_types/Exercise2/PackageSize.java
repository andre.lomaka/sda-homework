public enum PackageSize {
   SMALL(1, 2),
   MEDIUM(2, 3),
   LARGE(3, 4),
   UNKNOWN(0, 0);

   private final int minSize;
   private final int maxSize;

   PackageSize(int minSize, int maxSize) {
      this.minSize = minSize;
      this.maxSize = maxSize;
   }

   public static PackageSize getPackageSize(int size) {
      for (PackageSize ps : values()) {
         if (size <= ps.maxSize && size >= ps.minSize) return ps;
      }
      return UNKNOWN;
   }
}
