public class Exercise3 {
   public static void main(String[] args) {
      System.out.println("-40 C is " + TemperatureConvert.convertTemperature('C', 'F', -40.0f) + " F");
      System.out.println("0 C is " + TemperatureConvert.convertTemperature('C', 'K', 0.0f) + " K");
      System.out.println("0 K is " + TemperatureConvert.convertTemperature('K', 'C', 0.0f) + " C");
      System.out.println("-40 F is " + TemperatureConvert.convertTemperature('F', 'C', -40.0f) + " C");
      System.out.println("32 F is " + TemperatureConvert.convertTemperature('F', 'K', 32.0f) + " K");
      System.out.println("273.15 K is " + TemperatureConvert.convertTemperature('K', 'F', 273.15f) + " F");
   }
}
