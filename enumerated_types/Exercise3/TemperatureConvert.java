public enum TemperatureConvert {
   C_F('C', 'F', new Converter() { public float convert(float tempIn) {
                                      return tempIn*9.0f/5.0f + 32.0f;
                                   }
                                 }),
   C_K('C', 'K', new Converter() { public float convert(float tempIn) {
                                      return (tempIn + 273.15f);
                                   }
                                 }),
   K_C('K', 'C', new Converter() { public float convert(float tempIn) {
                                      return (tempIn - 273.15f);
                                   }
                                 }),
   F_C('F', 'C', new Converter() { public float convert(float tempIn) {
                                      return (tempIn - 32.0f)*5.0f/9.0f;
                                   }
                                 }),
   F_K('F', 'K', new Converter() { public float convert(float tempIn) {
                                      return (tempIn - 32.0f)*5.0f/9.0f + 273.15f;
                                   }
                                 }),
   K_F('K', 'F', new Converter() { public float convert(float tempIn) {
                                      return (tempIn - 273.15f)*9.0f/5.0f + 32.0f;
                                   }
                                 });

   private final char inputUnit;
   private final char outputUnit;
   private final Converter converter;

   TemperatureConvert(char inputUnit, char outputUnit, Converter converter) {
      this.inputUnit = inputUnit;
      this.outputUnit = outputUnit;
      this.converter = converter;
   }

   public static float convertTemperature(char inputUnit, char outputUnit, float value) {
      for (TemperatureConvert tc : values()) {
         if (tc.inputUnit == inputUnit && tc.outputUnit == outputUnit) {
            return tc.converter.convert(value);
         }
      }
      return 0.0f;
   }
}
