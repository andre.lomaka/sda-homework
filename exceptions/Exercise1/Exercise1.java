public class Exercise1 {
   public static double divide(double numerator, double denominator) throws CannotDivideBy0Exception {
      if (denominator == 0.0)
         throw new CannotDivideBy0Exception();
      return numerator/denominator;
   }

   public static void main(String[] args) {
      try {
         System.out.println(divide(1.0, 0.0));
      } catch (CannotDivideBy0Exception e) {
         System.err.println("Division by zero");
      } finally {
         System.out.println("Division finished");
      }
   }
}
