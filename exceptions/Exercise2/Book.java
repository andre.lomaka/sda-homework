import java.time.Year;

public class Book {
   private String id;
   private String title;
   private String author;
   private Year year;

   public Book(String id, String title, String author, int year) {
      this.id = id;
      this.title = title;
      this.author = author;
      this.year = Year.of(year);
   }

   public String getId() {
      return id;
   }

   public String getTitle() {
      return title;
   }

   public String toString() {
      return "id: " + id + "; title: " + title + "; author: " + author + "; year: " + year;
   }
}
