import java.util.List;
import java.util.ArrayList;

public class BookRepository {
   private List<Book> books;

   public BookRepository() {
      books = new ArrayList<>();
   }

   public void add(Book book) {
      books.add(book);
   }

   public Book remove(int position) {
      if (position >= books.size() || position < 0) return null;
      return books.remove(position);
   }

   public void printBookInfo() {
      for (Book b : books) {
         System.out.println(b.toString());
      }
   }

   public Book searchByTitle(String name) throws NoBookFoundException {
      for (int i=0; i < books.size(); i++) {
         Book b = books.get(i);
         if (b.getTitle().equals(name)) return b;
      }
      throw new NoBookFoundException("Couldn't find the book with title: " + name);
   }

   public Book searchById(String id) throws NoBookFoundException {
      for (int i=0; i < books.size(); i++) {
         Book b = books.get(i);
         if (b.getId().equals(id)) return b;
      }
      throw new NoBookFoundException("Couldn't find the book with id: " + id);
   }

   public Book removeById(String id) throws NoBookFoundException {
      for (int i=0; i < books.size(); i++) {
         Book b = books.get(i);
         if (b.getId().equals(id)) {
            return remove(i); 
         }
      }
      throw new NoBookFoundException("Couldn't find the book with id: " + id);
   }
}
