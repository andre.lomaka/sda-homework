public class Exercise2 {
   public static void main(String[] args) {
      BookRepository br = new BookRepository();
      br.add(new Book("1", "Title1", "Author1", 2001));
      br.add(new Book("2", "Title2", "Author2", 2002));
      br.add(new Book("3", "Title3", "Author3", 2003));
      br.add(new Book("4", "Title4", "Author4", 2004));
      try {
         br.removeById("5");
      } catch (NoBookFoundException e) {
         System.err.println(e);
      }
      br.printBookInfo();
   }
}
